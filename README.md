整理有关智慧城市相关的建设方案、国家标准、国家规范、解决方案和参考资料等，相关内容大多来源于互联网，收集整理方便项目经理、产品经理、相关领域从事者使用。因为个人从事四年都是在交通行业，主要是交通、智慧公路、智慧高速、公交优先、车路协同等的研究。所以将相关内容进行整理，方便查找和使用，同时也分享给有需要的人进行使用。

因为内容都是基于Markdown进行整理的，所以推荐使用Markdown工具进行阅读。

仓库地址： https://gitee.com/AiShiYuShiJiePingXing/smart-city.git

# 一、推荐阅读工具

Typora：一款轻便简洁的Markdown编辑器

官网地址：https://www.typora.io/

# 二、如何使用

1. 克隆文档到本地：``git clone  https://gitee.com/AiShiYuShiJiePingXing/smart-city.git`
2. 推荐使用Typora打开文件夹的方式打开，并设置Typora视图显示文件树

效果如下：

![image-20210914235244428](https://gitee.com/er-huomeng/l-img/raw/master/image-20210914235244428.png)

# 三、整理相关内容

![image-20210914235922265](https://gitee.com/er-huomeng/l-img/raw/master/image-20210914235922265.png)



<font color='red'>**目前个人重点研究对象：车路协同、智慧高速、智慧交通、智能网联车、车路协同云控平台。**</font>

# 说明：

- 整理资源不易，目前主要从事车路协同、交通行业的相关工作，所以每天都会阅读相关资料，为了方便快速查阅，才进行了总结。
- 欢迎智慧交通领域、智慧公路、公交优先、智慧高速、车路协同的朋友共同交流。

![image-20211022105818637](https://gitee.com/er-huomeng/l-img/raw/master/image-20211022105818637.png)



## 4 仓库操作记录

- 2022.04.26	-------------> 将车路协同、自动驾驶相关的内容全部移到另一个自动驾驶的仓库，那个仓库之后专门研究车路协同以及自动驾驶。
- 仓库地址：[自动驾驶: 整理自动驾驶相关的内容，为车路协同自动驾驶提供资料和笔记 (gitee.com)](https://gitee.com/AiShiYuShiJiePingXing/auto-driving)



本站文章引用或转载写明来源，感谢原作者的辛苦写作，如果有异议或侵权，及时联系我处理，谢谢！

更多车路协同，车路协同建设解决方案，参考链接：https://gitee.com/AiShiYuShiJiePingXing/auto-driving

个人网站：https://www.lovebetterworld.com/

- QQ：1090239782
- 微信：lovebetterworld
- 公众号：爱是与世界平行

![image-20210702094058887](https://gitee.com/AiShiYuShiJiePingXing/img/raw/master/img/image-20210702094058887.png)
