- [杭绍台智慧高速建设实践及思考建议 (qq.com)](https://mp.weixin.qq.com/s/xnk1Z2zWustfq7zzlYKlsg)

**杭绍台智慧高速建设实践**

杭绍台高速是连接杭州湾区和东部沿海产业带的南北向交通大动脉，气象和地质情况复杂，沿线峡谷桥梁、特长隧道众多，研究和实施杭绍台高速智慧化建设十分必要。杭绍台智慧高速整体架构为“1+3+3+N” ：1个云平台，3套智能支撑引擎，3个移动端APP，支撑N个业务应用和智慧化场景。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/wwwW1TFZY46gbEQuUkNCKQhoEMRUibm33LicyFzCsGO4n7O9y4HbPJo0TvjsXroQxpyP4QmgHKlKzrqfMSFWRCsQ/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

杭绍台智慧高速整体架构



**云系统架构**

浙江省交通运输厅《智慧高速公路建设指南》和浙江省国资委《浙江省国资国企数字化改革行动方案》，均对云平台建设提出了明确要求，需要统一规划，避免重复建设，加强数据互联互通和资源共享，打造稳定、安全、高性能、易拓展的企业数字化基础设施底座。

结合集团“十四五发展规划”和集团数字化改革行动方案的数字化建设要求，能平滑升级到企业云平台，基础底座和安全防护部分无需重复建设，打造基础设施一朵云、数据共享一盘棋、安全防护一张网，最终实现一切业务数据化，一切数据业务化。



**全息感知 海量多源数据管理**

完备的感知和控制体系。通过多种感知、控制和发布设备，加强对道路上车、路、环境的精细化感知交互。目前平台系统接入管理5000台以上设备，每天处理数据1000万/条以上。包括全路段的交通流检测（约1公里间隔）、全覆盖的视频监控和事件检测（主线200米间隔、隧道120米间隔） 、全路段的隧道监测数据、全区域的气象状态数据（20多个气象仪、全路段气象预报、视频能见度识别）、全覆盖的高精度地图和城区段30公里高精度定位、高密度的可变情报板（约1公里间隔）、团雾常发路段的智能雾灯（10公里）、重点车辆轨迹追踪的激光雷达（2公里）、重点高边坡和桥梁的健康监测、车路协同段试点（6公里单向RSU覆盖、2公里激光雷达和边缘计算）等。

多源数据接入。多源数据包括道路与隧道实时视频；路侧设备基础信息与状态；隧道内基础设备信息、状态等；天气信息、恶劣天气预警、基础采集信息，路况信息；高精/标精路网、地图瓦片等；桥梁、边坡检测指标、数据设备状态、数据标准；救援车辆信息；车辆、设备、道路信息；道路凝冰系统-道路气象信息等。

设备资产全周期管理。通过设备与路网绑定，支持设备地图定位。统一资产管理，涵盖设备基础信息、所在路网信息和管辖单位信息。



**全路段无盲区异常事件自动检测**

交通事件AI识别。交通事件AI识别可从视频监控转变为视频智能泛感知，实现交通事件由人工监控转变为自动感知+人工识别确认；可实现车道级的交通事件、交通参数的检测，自动检测、分析、评估道路实时状况，并与监控中心联动。检测范围覆盖车道、硬路肩、导流带、紧急停车带等，并可有效支持双向车道的检测。

视觉分析能力。主要包括交通异常事件（异常停车、行人或非机动车、异常行驶），道路异常事件（抛洒物、施工区域），交通参数（交通流量、运行车速、车间距、交通状态），设备异常（视频质量），辅助功能（云台联动控制、客货分流、火焰、路面能见度）。



**智慧化管控 事件闭环处置**

基于事件驱动的业务处理模式，通过一套算法一个平台，实现交通事件感知、预案决策、设备控制和信息发布，每次事件处置都形成了闭环，还能通过数据进行效果评价，不断提升优化算法模型和平台功能，从被动响应到主动管控，实现有监有控。

![图片](https://mmbiz.qpic.cn/mmbiz_jpg/wwwW1TFZY46gbEQuUkNCKQhoEMRUibm33D9J8mgorOPSOjSJQMqQRYDIcqKKpL3S4aq2TjTM8Yv2md3l5NQPJvw/640?wx_fmt=jpeg&wxfrom=5&wx_lazy=1&wx_co=1)

异常事件闭环处置



**智慧化数字大屏**

基于高精度地图，深度融合道路动静态要素，将设施设备、路况、事件、气象、隧道等交通信息统一汇聚，实现了高精度地图全覆盖、海量数据整合呈现、核心数据实时更新、业务系统协同联动。此外，杭绍台数字孪生实现了三维高精度地图覆盖、全要素孪生效果展现、历史问题回溯分析与未来趋势仿真预测。



**“X”智慧高速特色场景**

特色场景1：车路协同 辅助驾驶

通过车道级APP提供车路协同能力和服务，辅助驾驶员的驾驶行为，优化车辆在公路的驾乘安全和效率，提升智慧出行体验。

超视距感知提醒，给出行者提供超视距的信息提醒，发生危险更早告知智能网联车辆。准全天候通行，恶劣天气条件下，可以基于车载端导航进行安全驾驶，弥补人眼看不到的危险。伴随式信息服务，贴身伴随式信息发布和支付等服务，将网联车升级为一个大号智能手机。车路信息的双向互通，车辆重要信息与道路监管实时互通，能更好提供服务、加强监管、保障安全。

特色场景2：准全天候通行

实现全程覆盖的高精度地图采集，打造基础设施数字化，为车道级交通管理、伴随式交通服务、预防性资产维护提供高精度支撑。气象监测预警：气象仪、互联网气象数据、视频感知；智能分析决策：自动分析建议管控策略；智能诱导：一键下发控制雾灯、情报板等。

特色场景3：智慧服务区

在实现服务区常规功能的基础上，增加建设了视频监控(车辆管理、人流监测)、智慧厕所、能源监控、能耗管理、信息发布等系统应用，实现服务区人流、车流、卫生状况等指标全流程采集、监测，实时发布路况、停车位等动态信息，打造“人、车、区”交互的智慧服务区。

- 外场监测：基于视频识别功能，实现规范停车、可疑人员、区域越界等事件监测分析及预警。
- 省中心数据互联：基础数据按一定的采集频次上传省级平台，入区车流量、视频图像等信息实时上传省路网监测系统。
- 信息发布：基于移动app、诱导屏等手段实现服务区的智能导航，包括停车位、充电桩、加油站和厕所等实时信息。
- 智能公厕：公厕使用情况实时监测和对外发布，可通过发布屏进行厕所位置查找、占用情况查询等。

特色场景4：智慧隧道

通过特效灯光消除视觉疲劳，接入近3000台隧道设备，实现隧道交通、环境、火灾预案统一管理部署。

**杭绍台智慧高速思考及建议**

当前在智慧高速建设过程中，仍然存在功能使用率不高、数据稳定性不足、智能化体现不充分、设计内容不清晰及缺少组织保障等问题。对于杭绍台智慧高速发展，有三个关键词：持续深入、开拓创新、保障机制。

持续深入。第一，加强异常事件、天气及重点车辆的识别和精准管控，逐步落实车道级主动管控能力；第二，全面应用智能施工管理和播报，规范施工操作流程；第三，继续探索泛车路协同体系应用，丰富公众信息发布内容，探索车道级导航播报；第四，持续探索数字孪生在智慧高速的应用，提升高速场景大数据分析应用能力；第五，持续探索与省厅、交警、交通的数据交互共享和业务系统场景。

开拓创新。第一，资产设施设备全生命周期管理，覆盖规划设计、建设施工、运营管理、运维养护，各阶段建立数据监测分析系统，数据持续沉淀积累，用于结构健康预警和养护决策支持；第二，移动工作台APP，丰富移动端工作APP内容，基于角色权限分配不同的查看、记录、审批、处置的功能，针对领导提供移动驾驶仓功能。

保障机制。第一，智慧高速的业务应用会对传统运营管控流程带来影响，一方面要基于实际业务需求优化应用，同时也需要保证配套的流程机制和提升人员能力；第二，一路多方协同需要进一步深入，智慧高速需要与协同部门协商探讨数据交换对接机制。

基于杭绍台智慧高速建设实践，对于智慧高速项目建议包括三个方面：一是，项目方案要考虑软硬结合，要机电设备配合业务应用，可以分开招标，但需要对机电专项提要求，比如摄像头密度和分辨率、情报板间隔距离和样式、用能监测数据等；二是，项目方案要综合实用创新，实用的功能需求需要确定可量化评估指标，创新的功能需求需要确定技术可行性和可复制推广空间；三是，项目组织要工作边界清晰，新建高速还是已有高速的需求，要和路段管理部门对齐，招标主体公司、运营公司业务部门、运营公司下属路段都要参与方案讨论和确认，且工作边界和对接流程清晰。